#include <stdio.h>
#include <stdlib.h>
#include <math.h>


struct nodo{
      int dato;
      struct nodo *sig;
};

//variable global que apunta al primer nodo de la pila
struct nodo	*raiz = NULL;

void insertar(int x){
struct nodo *nuevo;
	nuevo = malloc(sizeof(struct nodo));
	nuevo->dato = x;
	if(raiz	== NULL){
		raiz = nuevo;
		nuevo->sig == NULL;
	}
	else{
		nuevo->sig = raiz;
		raiz = nuevo;
	}
}
//Muestra el numero invertido
void imprimir(){
	struct nodo *reco=raiz;
	printf("Número invertido es : \n");
	while(reco!=NULL){
		printf("%i",reco->dato);
		reco=reco->sig;
	}
	printf("\n");
}

//Se le pide al usuario que ingrese el numero que desea invertir
void main(){
	int numero;
	char * cad = malloc(12 * sizeof(char));
	printf("Ingrese un número:\n");
	scanf("%d", &numero);
	sprintf(cad, "%i", numero);
		for(int i=0;cad[i]!='\0';i++){
			int n = (cad[i] - '0');
			insertar(n);
		}
	imprimir();	
}

