#include <stdio.h>
#include <stdlib.h>

struct nodo{
	int info;
	struct nodo *sig;
};

struct nodo	*raiz = NULL;

void reemplazar(struct nodo *pila, int viejo, int nuevo){
	if(pila!= NULL){
		if(pila->info == viejo){
			pila->info = nuevo;
		}
		else{
			reemplazar(pila->sig,viejo,nuevo);
		}
	}
	else{
		printf("No existen coincidencias.\n");
	}
}

void ingresar(int x){
struct nodo *nuevo;
	nuevo = malloc(sizeof(struct nodo));
	nuevo->info = x;
	if(raiz	== NULL){
		raiz = nuevo;
		nuevo->sig == NULL;
	}
	else{
		nuevo->sig = raiz;
		raiz = nuevo;
	}
}
void imprimir(){
	struct nodo *reco=raiz;
	printf("Lista completa:\n");
	while(reco!=NULL){
		printf("%i ",reco->info);
		reco=reco->sig;
	}
	printf("\n");
}
//Dar a conocer si la pila se encuentra vacia o no
void seEncuentraVacia(){
	if(raiz == NULL){
		printf("La pila está vacía\n");
	}else{
		printf("La pila no está vacía\n");
	}
}
    
int main(){
	ingresar(20);
	ingresar(70);
	ingresar(4);
	imprimir();
	printf("Reemplazando elemento de la pila.\n");
	reemplazar(raiz,20,30);
	imprimir();
	return 0;
}

